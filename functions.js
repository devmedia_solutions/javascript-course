// ***********************************
// Navigation menu for mobile & tablet
// ***********************************
const slideMenu = () => {
  const hamburgerIcon = document.querySelector(".hamburger-menu");
  const navigationMenu = document.querySelector(".menu");
  const menuLinks = document.querySelectorAll(".menu li");

  hamburgerIcon.addEventListener("click", () => {
    navigationMenu.classList.toggle("menu-active");

    // Animate menu links
    menuLinks.forEach((link, index) => {
      if (link.style.animation) {
        link.style.animation = "";
      } else {
        link.style.animation = `menuItemFade 0.5s ease forwards ${index / 5 +
          0.5}s`;
      }
    });
    // Toggle hamburger menu
    hamburgerIcon.classList.toggle("toggle");
  });
};

slideMenu();

// ****************************
// Carousel for welcome screen
// ****************************
const carouselSlide = document.querySelector(".carousel-slides");
const carouselImages = document.querySelectorAll(".carousel-slides img");

const previousBtn = document.querySelector("#previous");
const nextBtn = document.querySelector("#next");

// Counter
let counter = 1;
const size = carouselImages[0].clientWidth;

carouselSlide.style.transform = "translateX(" + -size * counter + "px)";

// Click to change image
nextBtn.addEventListener("click", () => {
  if (counter >= carouselImages.length - 1) return;
  carouselSlide.style.transition = "transform 0.4s ease-in-out";
  counter++;
  carouselSlide.style.transform = "translateX(" + -size * counter + "px)";
});

previousBtn.addEventListener("click", () => {
  if (counter <= 0) return;
  carouselSlide.style.transition = "transform 0.4s ease-in-out";
  counter--;
  carouselSlide.style.transform = "translateX(" + -size * counter + "px)";
});

// Loop to the beginning
carouselSlide.addEventListener("transitionend", () => {
  if (carouselImages[counter].id === "lastClone") {
    carouselSlide.style.transition = "none";
    counter = carouselImages.length - 2;
    carouselSlide.style.transform = "translateX(" + -size * counter + "px)";
  }
  if (carouselImages[counter].id === "firstClone") {
    carouselSlide.style.transition = "none";
    counter = carouselImages.length - counter;
    carouselSlide.style.transform = "translateX(" + -size * counter + "px)";
  }
});

// **************************
// Modal with contact form
// **************************
const modalButton = document.querySelector(".contact-me");
const modalBg = document.querySelector(".modal-bg");
const modalClose = document.querySelector(".close-modal");

modalButton.addEventListener("click", () => {
  modalBg.classList.add("modal-bg-appear");
});

modalClose.addEventListener("click", () => {
  modalBg.classList.remove("modal-bg-appear");
});

// ********************************************
// Smooth scroll feature / Scroll to element
// ********************************************
const smoothScrool = (targetItem, duration) => {
  const target = document.querySelector(targetItem);
  const targetPosition = target.getBoundingClientRect().top;
  const startPosition = window.pageYOffset;
  const distance = targetPosition - startPosition;
  let startTime = null;

  // requestAnimationFrame() will loop animation() so we can get 60 FPS
  const animation = (currentTime) => {
    if (startTime === null) {
      startTime = currentTime;
    }
    const timeElapsed = currentTime - startTime;
    const run = ease(timeElapsed, startPosition, distance, duration);

    window.scrollTo(0, run);

    if (timeElapsed < duration) {
      requestAnimationFrame(animation);
    }
  }
  // Crazy mathematical function ;)
  // For more visit gizma.com/easing/ and there you can find more scroll patterns
 const ease = (t, b, c, d) => {
    t /= d / 2;
    if (t < 1) return (c / 2) * t * t * t + b;
    t -= 2;
    return (c / 2) * (t * t * t + 2) + b;
  }

  requestAnimationFrame(animation);
}

// Click to scroll on element
const scrollToAbout = document.getElementById("scrollToAbout");
scrollToAbout.addEventListener("click", () => {
  smoothScrool("#target-box", 1000);
});
// Click to scroll on top
const clickToTop = document.getElementById("scrollToTop");
clickToTop.addEventListener("click", () => {
  smoothScrool("#target-top", 1000);
});

// **********************************
// About text show on scroll
// **********************************
const scrollToAppear = () => {
  const content = document.querySelector(".text-content");
  const position = content.getBoundingClientRect().top;
  const screenPosition = window.innerHeight / 1.2;

  if (position < screenPosition) {
    content.classList.add("text-content-appear");
  }
}
// Adding scroll event that will trigger scrollToAppear() fucntion
window.addEventListener("scroll", scrollToAppear);

// **********************************
// Gallery animation
// **********************************
